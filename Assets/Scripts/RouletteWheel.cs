﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RouletteWheel : MonoBehaviour
{

    public Button spinButton;

    [Tooltip("Sum of probabilities of all prizes should equal one")]
    public RoulettePrize[] PrizePool = new RoulettePrize[8];
    public GameObject GetButton;
    public float spinSpeed = 1000;
    public float minSpeed = 50;

    private CanvasGroup canvasGroup;
    private GameObject collectedPrize;

    private void Start()
    {
        canvasGroup = GetComponentInParent<CanvasGroup>();
    }

    private int RandomPrize()
    {
        //This function assumes that sum of probabilities will equal 1
        //To normalize the probability if sum of probabilities could be > 1, use the following:
        //float max = 0;
        //foreach(RoulettePrize prize in PrizePool)
        //{
        //    max += prize.probability;
        //}
        //float randValue = Random.Range(0, max);
        
        float randValue = Random.value;
        float bucket = 0.0f;
        for (int i = 0; i < PrizePool.Length; i++)
        {
            bucket += PrizePool[i].probability;
            if (randValue <= bucket)
            {
                return i;
            }
        }
        return -1;
    }

    public void OutputToConsole()
    {
        Dictionary<RoulettePrize.Prize, int> output = new Dictionary<RoulettePrize.Prize, int>();
        Dictionary<RoulettePrize, int> buckets = new Dictionary<RoulettePrize, int>();
        
        //Initialize Dictionary
        foreach (RoulettePrize obj in PrizePool)
        {
            buckets[obj] = 0;
            output[obj.prize] = 0;
        }


        for(int i = 0; i < 1000; i++)
        {
            int prize = RandomPrize();
            output[PrizePool[prize].prize] += PrizePool[prize].quantity;
            buckets[PrizePool[prize]]++;
        }
        Debug.Log($"Items won:");
        foreach(KeyValuePair<RoulettePrize.Prize, int> kvp in output)
        {
            Debug.Log($"{kvp.Key}: {kvp.Value}");
        }
        Debug.Log($"Distribution of buckets:");
        foreach(KeyValuePair<RoulettePrize, int> kvp in buckets)
        {
            Debug.Log($"{kvp.Key.name}: {kvp.Value}");
        }
    }

    public void Spin()
    {
        int prize = RandomPrize();
        transform.eulerAngles = Vector3.zero;
        
        StartCoroutine("Spinning", prize);
    }

    IEnumerator Spinning(int prize)
    {

        float spinAmount = 1822.5f + (prize * 45f);

        float z = 0f;
        float speedDecay = spinSpeed;
        while(z < spinAmount)
        {
            z += Time.deltaTime * speedDecay;
            transform.eulerAngles = new Vector3(0, 0, z);
            speedDecay = Mathf.Lerp(spinSpeed, minSpeed, z/spinAmount);
            yield return null;
        }
        transform.eulerAngles = new Vector3(0, 0, spinAmount);

        collectedPrize = Instantiate(transform.GetChild(prize), transform).gameObject;
        collectedPrize.AddComponent<CanvasGroup>().ignoreParentGroups = true;

        float totalTime = 2f;
        float time = totalTime;

        Vector2 maxScale = new Vector2(3,3);
        Vector2 startScale = collectedPrize.transform.localScale;

        Vector2 startPosition = collectedPrize.transform.localPosition;
        Vector2 endPosition = Vector2.zero;



        while (time > 0)
        {
            time -= Time.deltaTime;
            canvasGroup.alpha = (time / totalTime/2) > 0.0001 ? time/totalTime/2 : 0.0001f;

            collectedPrize.transform.localScale = Vector2.Lerp(maxScale, startScale, time / totalTime);
            collectedPrize.transform.localPosition = Vector2.Lerp(endPosition, startPosition, time / totalTime);

            yield return null;
        }
        GameObject getButton = Instantiate(GetButton, collectedPrize.transform);
        getButton.transform.localScale = new Vector2(0.1f, 0.1f);
        getButton.transform.localPosition = new Vector2(0, -15);
        Button button = getButton.GetComponent<Button>();
        button.onClick.AddListener(delegate { Reset(); });
    }

    public void Reset()
    {
        Destroy(collectedPrize);
        canvasGroup.alpha = 1;
        spinButton.interactable = true;
    }
}
