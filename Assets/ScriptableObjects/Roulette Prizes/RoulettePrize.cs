﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Roulette Prizes")]
public class RoulettePrize : ScriptableObject
{
    public enum Prize
    {
        hammer,
        heart,
        gem,
        coin,
        brush
    }

    [Tooltip("Probability of all RoulettePrizes should equal to 1")]
    public float probability;

    public Prize prize;
    public int quantity;



}
